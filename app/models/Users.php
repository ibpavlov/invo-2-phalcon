<?php
namespace Invo\Models;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class Users extends Model
{
    public function validation()
    {
        $validator = new Validation;

        $validator->add(
            'email',
            new EmailValidator()
        );

        $validator->add(
            'username',
            new UniquenessValidator(
                [
					'model'   => $this,
                    'message' => 'Sorry, That username is already taken',
                ]
            )
        );

        return $this->validate($validator);
    }
}
