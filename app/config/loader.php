<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerNamespaces([
		'Invo\Controllers' => APP_PATH . $config->application->controllersDir,
		'Invo\Plugins' => APP_PATH . $config->application->pluginsDir,
		'Invo\Libraries' => APP_PATH . $config->application->libraryDir,
		'Invo\Models' => APP_PATH . $config->application->modelsDir,
		'Invo\Forms' => APP_PATH . $config->application->formsDir,
])->register();
