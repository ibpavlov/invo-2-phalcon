<?php
namespace Invo\Controllers;

use Phalcon\Flash;
use Phalcon\Session;

use Invo\Models\Users;

class InvoicesController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Manage your Invoices');
        parent::initialize();
    }

    public function indexAction()
    {
    }

    /**
     * Edit the active user profile
     *
     */
    public function profileAction()
    {
        //Get session info
        $auth = $this->session->get('auth');

        //Query the active user
        $user = Users::findFirst($auth['id']);
        if ($user == false) {
            return $this->forward('index/index');
        }

        if (!$this->request->isPost()) {
            $this->tag->setDefault('name', $user->name);
            $this->tag->setDefault('email', $user->email);
            $this->tag->setDefault('username', $user->username);
            $this->tag->setDefault('password', "");
            $this->tag->setDefault('repeatPassword', "");
        } else {

            $name = $this->request->getPost('name', array('string', 'striptags'));
            $email = $this->request->getPost('email', 'email');
            $username = $this->request->getPost('username', 'alphanum');
            $password = $this->request->getPost('password');
            $repeatPassword = $this->request->getPost('repeatPassword');

			if(!empty($password) || !empty($repeatPassword)) {
	            if ($password != $repeatPassword) {
	                $this->flash->error('Passwords are different');
					return true;
	            }
			}

            $user->name = $name;
            $user->email = $email;
			$user->username = $username;
			if(!empty($password) || !empty($repeatPassword)) {
				$user->password = sha1($password);
			}
            if ($user->save() == false) {
                foreach ($user->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
            } else {
                $this->flash->success('Your profile information was updated successfully');
            }
        }
    }
}
